Project Set up / Overview
    SearchBar folder contains the spec/test file “PositiveScenraios.spec.js”
    Page Objects folder contains the page objects as I have used the POM design pattern to separate the tests and the application level objects
    Report folder contains the html test report which is generated using mochawesome package and the assets folder under
    it contains the screenshots, CSS etc required to render the interactive html report.Video folder contains the test recording.
    Cypress.json & package.json are the config setting folders

CLI Command to execute tests
    CLI Command to run test and Logs: 
    cypress run "--spec" "cypress/integration/SearchBar/PositiveScenarios.spec.js" "-b" "chrome" "--reporter" "mochawesome”
    "--spec" "cypress/integration/SearchBar/PositiveScenarios.spec.js" : specifies which spec/test file to run
    "-b" "chrome"  : specifies which browser to run
    "--reporter" "mochawesome” : specifies which reporter to use for generating test reports

HTML Test Reporting and Video Recording
    The generated report looks like this and this has been checked into the repo as well.
    I have enabled screenshots even for the tests which are passing only for the demo purpose 
    and this setting is configurable.The video recording of the tests can be found under the video folder.
    