import PageManager from '../../pageObjects/PageManager'
import Helper from "../../utils/Helper";

/*********** this spec file contains component level tests for the search bar ********
 * and these component tests have been orrganised in a sequence to
 * provide coverage for E2E customer search flow.
 * Author: AshishT
 * Date:03/05/21
* ***********************************************************************************/

before(function()
{
    cy.intercept('GET', '**/?Platform=V2').as('waitForRedirect')
    cy.visit(Cypress.env('testURL'))
})

// this desc block contains 5 tests
describe("End_to_End_Scenarios", function()
{
  const pgManager = new PageManager()

    it("Redirect_is_successful",function()
    {
        cy.viewport(1980,1080)
        cy.wait('@waitForRedirect').then(xhr => expect(xhr.response.statusCode).to.be.oneOf([301,302,307]))
        cy.waitUntil( ()=> cy.url().should("not.contain","?Platform=V2"))
        cy.screenshot()
    })
    it("verify_deafultext_and_searchbox_enabled", function()
    {  
        cy.viewport(1980,1080)
        pgManager.getHomePage().getPlaceHolderText().should('have.attr','placeholder','Search products and more')
        pgManager.getHomePage().getPlaceHolderText().should('not.be.disabled')
        cy.screenshot()
    })

    it("searchIcon_is_diplayed", function()
    {
        cy.viewport(1980,1080) 
        pgManager.getHomePage().getsearchIcon().should('be.visible')
        cy.screenshot()
    })

    it("flyoutPanel_is_displayed", function()
    {
        cy.viewport(1980,1080)
        pgManager.getHomePage().getPlaceHolderText().click({ force: true })
        pgManager.getHomePage().getFlyout().should('be.visible')
        cy.screenshot()
    })

    it("verify_defaultHeadings_in_flyout", function()
    {
        cy.viewport(1980,1080)
        cy.contains('Popular searches').should('be.visible')
        cy.contains('Popular right now').should('be.visible')
        cy.screenshot()
    })

    it("search_product_verify_searchTerm_in_resultHeading", function()
    {
        cy.intercept('POST', '**/v1/search/global').as('getSearchResults')
        cy.viewport(1980,1080)
        
        pgManager.getHomePage().getPlaceHolderText().type('bbq{enter}')

        cy.wait('@getSearchResults').its('response.statusCode').should('eq',200)
        cy.waitUntil( ()=> cy.url().should("contain","&page=1"))
        cy.get('.searchTerm > h2 > span').invoke("text").should('eq', 'bbq');
        cy.screenshot()
    })

    





}) // end of describe