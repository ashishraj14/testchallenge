export default class Helper
{

    /*  @param and @returnType are none
    *  this function clears the cookies
    * */
    static clearCookies()
    {
        cy.clearCookies({log: true})
    }

    /*  @param - none
    *   @returnType - screenshot object containing the full page view
    * */
    static takeFullPageSccreenshot()
    {
        return cy.screenshot({capture:'fullPage'})
    }

}