const searchPlaceHolderText = '#custom-css-outlined-input'
const searchIcon = '.search-icon-wrapper > .Icon__SVG-akqoq7-0'
const flyOutPanel = '#flyout'

/**************************************************************
 this class represents the home/landing page for bunnings.com.au
 Author:AshishT
 ****************************************************************/
export default class HomePage
{
    getPlaceHolderText()
   {
       return cy.get(searchPlaceHolderText)
   }

   getsearchIcon()
   {
       return cy.get(searchIcon)
   }

   getFlyout()
   {
    return cy.get(flyOutPanel) 
   }

   



} // end of class

