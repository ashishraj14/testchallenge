import HomePage from '../pageObjects/HomePage'

/***************************************************************************************************************
 this class manages/provides poge objects to all the tests
 All page objects and their member methods in the .spec files will be accessible by using the following synTAX
 pageManagerObj.getHomePage()

 Author:AshishT
 ****************************************************************************************************************/

export default class PageManager
{
    getHomePage()
    {
        return new HomePage()
    }


    
}// end of class